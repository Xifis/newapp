const express = require('express')
// SIMPLE SERVER : ALLOWS TO SETUP MIDLEWARES, DATA REQUESTS + CREATION OF API
const bodyParser = require('body-parser')
// JSON PARSER, HANDLES RAW TYPES OF DATA
const cors = require('cors')
// CROSS SECURITY DOMAIN + ANY CLIENT
const morgan = require("morgan")
// HTTP REQUEST LOGGER
// const config = require("./config/config")


const app = express()
app.use(morgan('combined'))
app.use(bodyParser.json())

// CORS (Cross-Origin Resource Sharing) headers to support Cross-site HTTP requests 
// ........apparently not working........
// app.use(cors(
//   {
//   methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
//   credentials: true,
//   preflightContinue: false,
//   origin: ['http://localhost:9091', '*'],
// }
// ))

app.get('/status', (req, res) => {
    res.send({
        message: 'Server is up and running'
    })
})

app.listen(process.env.PORT || 8081)